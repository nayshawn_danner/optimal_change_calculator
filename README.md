# Change Calculator App #
Simple rest service that calulates optimal change combinations

## Setup ##
Change Calculator App was built using [scala 2.11.8](http://www.scala-lang.org/download/) (_required to_ _**Build & Run**_) and [sbt version 0.13.11](http://www.scala-sbt.org/0.13/docs/Setup.html)

## Build & Run ##

```sh
$ cd change_calculator_app
$ ./sbt
> jetty:start
```

## Usage ##
```sh
$ http localhost:8080/optimal/?amount=12.85
 HTTP/1.1 200 OK
 Content-Length: 93
 Content-Type: application/json; charset=UTF-8
 Date: Sun, 12 Jun 2016 17:42:58 GMT
 Server: Jetty(9.2.1.v20140609)
 
 {
     "optimal_change": {
         "dime": 1,
         "half_dollar": 1,
         "quarter": 1,
         "silver_dollar": 12
     },
     "total": "$12.85"
 }
```
_note: example uses [httpie](https://github.com/jkbrzt/httpie) for rest call feel free to use cUrl_

## Testing ##
An **sbt** executable was provided in the project to simplify testing:
```sh
$ cd change_calculator_app
$ ./sbt test
```