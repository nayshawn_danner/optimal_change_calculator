import javax.servlet.ServletContext

import com.dannernaytion.changecalculator.api._
import com.dannernaytion.changecalculator.application.ChangeCalculatorApp
import com.dannernaytion.changecalculator.core.{ChangeCalculator, OptimalChangeCalculatorComponent}
import org.scalatra._

class ScalatraBootstrap extends LifeCycle
  with OptimalChangeCalculatorComponent
{
  override val calculator: ChangeCalculator =
    new RecursiveOptimalChangeCalculator
//    override val calculator: ChangeCalculator =
//      new FunctionalOptimalChangeCalculator

  override def init(context: ServletContext) {

    implicit val application = ChangeCalculatorApp(calculator)

    context.mount(new ChangeCalculatorController, "/*")
  }

}
