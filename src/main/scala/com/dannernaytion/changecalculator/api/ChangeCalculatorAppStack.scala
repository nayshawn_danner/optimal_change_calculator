package com.dannernaytion.changecalculator.api

import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json.JacksonJsonSupport

trait ChangeCalculatorAppStack extends ScalatraServlet with JacksonJsonSupport {

  notFound {

  }

  override protected implicit val jsonFormats: Formats = DefaultFormats
  before(){
    contentType = formats("json")
  }
}
