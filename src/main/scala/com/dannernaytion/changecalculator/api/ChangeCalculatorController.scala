package com.dannernaytion.changecalculator.api

import com.dannernaytion.changecalculator.application.ChangeCalculatorApp
import org.json4s.JValue
import org.json4s.JsonAST.{JInt, JString}

import scala.util.Try

class ChangeCalculatorController(implicit val application: ChangeCalculatorApp)
  extends ChangeCalculatorAppStack {

  get("/optimal") {

    params.get("amount") match {
      case Some(amount) => amount.parseDouble match {
          case Some(amount) => application.calculateOptimal(amount)
          case None => status = 400; new ApiErrorResponse(invalidAmountError)
        }
      case None => status = 400; new ApiErrorResponse(amountRequiredError)
    }
  }
  override protected def transformResponseBody(body: JValue): JValue = {
    body transformField {
      case ("change", x) => ("optimal_change", x.removeField {
                                case (_, JInt(x)) => x == BigInt(0)
                                case _ => false
                            })
      case ("total", x) => ("total", JString(s"$$${x.extract[String]}"))
    }
  }

  val amountRequiredError = ApiError(`type`="AMOUNT_REQUIRED", message="amount required to calculate optimal change")
  val invalidAmountError = ApiError(`type`="INVALID_AMOUNT", message="valid amount required to calculate optimal change")

  case class ApiErrorResponse(`error`: ApiError)
  case class ApiError(`type`:String, message:String)
  implicit class StringExtension(val value: String){
    def parseDouble: Option[Double] = Try { value.toDouble }.toOption
  }

}

