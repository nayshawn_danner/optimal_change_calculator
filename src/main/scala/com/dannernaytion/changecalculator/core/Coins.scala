package com.dannernaytion.changecalculator.core


object Coins {
  val allCoins: Set[Coin] = Set(Penny,Nickle,Dime,Quarter,HalfDollar,SilverDollar)

  sealed abstract class Coin(val value:Double) extends Ordered[Coin] {
    override def compare(that: Coin): Int = {
      return (value * 100).toInt - (that.value * 100).toInt
    }
  }

  case object None extends Coin(.00)
  case object Penny extends Coin(.01)
  case object Nickle extends Coin(.05)
  case object Dime extends Coin(.10)
  case object Quarter extends Coin(.25)
  case object HalfDollar extends Coin(.50)
  case object SilverDollar extends Coin(1.00)

  def apply(amount: Double): Coin = amount match {
    case Penny.value        => Penny
    case Nickle.value       => Nickle
    case Dime.value         => Dime
    case Quarter.value      => Quarter
    case HalfDollar.value   => HalfDollar
    case SilverDollar.value => SilverDollar
    case _                  => None
  }

}
