package com.dannernaytion.changecalculator.core

import math.rint
import com.dannernaytion.changecalculator.core.Coins.{Coin, None}

trait OptimalChangeCalculatorComponent {

  val calculator: ChangeCalculator

  class FunctionalOptimalChangeCalculator extends ChangeCalculator
  {
    import Monoid._

    override def calculate(forAmount: Double, withCoins: Set[Coin]): ChangeHolder = {
      val sortedCoins: List[Coin] = withCoins.toList.sortBy(-_.value)

      val compose = composeMonoid[Double, Map[Coin, BigInt]](
        doubleSubtractionMonoid(forAmount),
        mergeMapMonoid[Coin, BigInt](bigIntAdditionMonoid)
      )
      val coinMap: Map[Coin, BigInt] =
        sortedCoins.foldLeft(compose.zero) {
          (accum, coin) => compose.op(accum, reduce(accum._1, coin))
        }._2

      return coinMap.foldLeft(ChangeHolder(None)) { (holder: ChangeHolder, kv) => holder + ChangeHolder(kv._1, kv._2) }
    }

    def reduce(amount: Double, coin: Coin): (Double, Map[Coin, BigInt]) = {
      val coinCount: BigInt = (BigDecimal(amount) / coin.value).toBigInt()
      if (coinCount == 0) return (0.0, Map())
      val reduction = coinCount.doubleValue() * coin.value
      val reductionRounded: Double = rint(reduction * 100) / 100

      (reductionRounded, Map(coin -> coinCount))
    }
  }

  class RecursiveOptimalChangeCalculator extends ChangeCalculator
  {
    override def calculate(
                            forAmount: Double,
                            withCoins: Set[Coin] = Coins.allCoins
                          ): ChangeHolder = {
      val sortedCoins: List[Coin] = withCoins.toList.sortBy(-_.value)
      calculateChange(forAmount, sortedCoins, ChangeHolder(None))
    }

    private def calculateChange(amount: Double,
                                coins: List[Coin],
                                change: ChangeHolder
                               ): ChangeHolder =
      (amount <= 0, coins.head, coins.tail) match {
        case (true, _, _) => change
        case (_, coin, tail) =>
          val coinCount = (BigDecimal(amount) / coin.value).toBigInt()
          if (coinCount == 0) calculateChange(amount, tail, change)
          else {
            val reduction = coinCount.doubleValue() * coin.value
            val reducedAmount = amount - reduction
            val roundedAmountRounded = rint(reducedAmount * 100) / 100
            calculateChange(
              roundedAmountRounded,
              coins,
              change + ChangeHolder(coin, coinCount))
          }
      }
  }
}

object Monoid
{
  trait Monoid[T] {
    def zero(): T

    def op(l: T, r: T): T
  }

  def composeMonoid[K, T](a: Monoid[K], b: Monoid[T]): Monoid[(K, T)] =
    new Monoid[(K, T)] {
      val zero: (K, T) = (a.zero(), b.zero())

      override def op(l: (K, T), r: (K, T)): (K, T) =
        (a.op(l._1, r._1), b.op(l._2, r._2))

    }

  def doubleSubtractionMonoid(amount: Double): Monoid[Double] = {
    new Monoid[Double] {
      val zero: Double = amount

      override def op(l: Double, r: Double): Double = rint((l - r) * 100) / 100
    }
  }

  def bigIntAdditionMonoid(): Monoid[BigInt] =
    new Monoid[BigInt] {
      val zero: BigInt = BigInt(0)

      override def op(l: BigInt, r: BigInt): BigInt = l + r
    }

  def mergeMapMonoid[K, V](a: Monoid[V]): Monoid[Map[K, V]] =
    new Monoid[Map[K, V]] {
      val zero: Map[K, V] = Map()

      override def op(l: Map[K, V], r: Map[K, V]): Map[K, V] =
        (l.keySet ++ r.keySet).foldLeft(zero) {
          (accum, key) => accum + (key -> a.op(l.getOrElse(key, a.zero()), r.getOrElse(key, a.zero())))
        }
    }
}