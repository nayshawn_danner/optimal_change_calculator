package com.dannernaytion.changecalculator.core
import com.dannernaytion.changecalculator.core.Coins.Coin

/**
  * Created by nayshawn on 6/10/16.
  */
trait ChangeCalculator {
  def calculate(forAmount: Double,
                withCoins: Set[Coin] = Coins.allCoins
               ): ChangeHolder
}
