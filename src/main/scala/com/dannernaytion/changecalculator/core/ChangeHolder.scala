package com.dannernaytion.changecalculator.core

import com.dannernaytion.changecalculator.core.Coins.Coin

sealed trait ChangeHolder {
  def +(other: ChangeHolder): ChangeHolder

  def coinCount(forCoin: Coin): BigInt

  def coinValue():Double
}

object ChangeHolder {
  def apply(coin: Coin,count:BigInt = 1): ChangeHolder =
    new ChangeHolderImpl(Map(coin -> count))

  private class ChangeHolderImpl(val holder: Map[Coin,BigInt]) extends ChangeHolder
  {
    override def coinCount(coin: Coin): BigInt =
      if (holder.contains(coin)) holder(coin) else 0

    def +(other: ChangeHolder): ChangeHolder =
    {
      val otherChangeHolder: ChangeHolderImpl = other.asInstanceOf[ChangeHolderImpl]

      if(otherChangeHolder.coinValue() == 0)
        return new ChangeHolderImpl(holder)
      if(coinValue() == 0)
        return new ChangeHolderImpl(otherChangeHolder.holder)

      val otherHolder: Map[Coin,BigInt] = otherChangeHolder.holder
      val tempHolder: Map[Coin, BigInt] = holder.foldLeft(otherHolder)(
        (acc, kv) =>
          acc + (kv._1 ->
            (if (acc.contains(kv._1)) acc(kv._1) + kv._2 else kv._2))
      )

      return new ChangeHolderImpl(tempHolder)
    }

    override def coinValue(): Double = {
      val rawValue: Double = holder.foldLeft(0.00)(
        (acc, kv) => acc + kv._1.value * kv._2.doubleValue()
      )
      (math rint rawValue * 100 ) / 100
    }



    def canEqual(other: Any): Boolean = other.isInstanceOf[ChangeHolderImpl]
    override def equals(other: Any): Boolean = other match {
      case that: ChangeHolderImpl =>
        (that canEqual this) &&
          holder == that.holder
      case _ => false
    }
    override def hashCode(): Int = {
      val state = Seq(holder)
      state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
    }
    override def toString = s"ChangeHolderImpl($holder, $coinValue)"
  }
}
