package com.dannernaytion.changecalculator.application

import com.dannernaytion.changecalculator.core.Coins._
import com.dannernaytion.changecalculator.core.{ChangeCalculator, ChangeHolder, Coins}


class ChangeCalculatorAppImpl(val changeCalculator: ChangeCalculator) extends ChangeCalculatorApp {

  override def calculateOptimal(amount: Double): ChangeDetail = {
    val changeHolder: ChangeHolder = changeCalculator.calculate(amount, Coins.allCoins)
    new ChangeDetail(
      new Change(
        penny = changeHolder.coinCount(Penny),
        nickle = changeHolder.coinCount(Nickle),
        dime = changeHolder.coinCount(Dime),
        quarter = changeHolder.coinCount(Quarter),
        half_dollar = changeHolder.coinCount(HalfDollar),
        silver_dollar = changeHolder.coinCount(SilverDollar)),
      changeHolder.coinValue())
  }
}

trait ChangeCalculatorApp {
  def calculateOptimal(amount: Double):ChangeDetail
}

object ChangeCalculatorApp {
  def apply(changeCalculator: ChangeCalculator): ChangeCalculatorApp =
    new ChangeCalculatorAppImpl(changeCalculator)
}

case class ChangeDetail(
                         val change: Change,
                         val total: Double
                       )
case class Change(
                   val silver_dollar: BigInt = 0,
                   val half_dollar: BigInt = 0,
                   val quarter: BigInt = 0,
                   val dime: BigInt = 0,
                   val nickle: BigInt = 0,
                   val penny: BigInt = 0
                 )

