package com.dannernaytion.changecalculator.core

class FunctionalOptimalChangeCalculatorSpec
  extends OptimalChangeCalculatorSpec
{
  override val calculator: ChangeCalculator =
    new FunctionalOptimalChangeCalculator
}
