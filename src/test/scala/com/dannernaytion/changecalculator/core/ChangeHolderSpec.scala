package com.dannernaytion.changecalculator.core

import com.dannernaytion.changecalculator.core.Coins._
import org.scalatest.{FlatSpec, GivenWhenThen, ShouldMatchers}

/**
  * Created by nayshawn on 6/4/16.
  */
class ChangeHolderSpec extends FlatSpec with ShouldMatchers with GivenWhenThen {

  "A ChangeHolder" must "hold the coin it is initialized with" in {
    ChangeHolder(Penny).coinCount(Penny) should be (1)
  }

  it should "contain coins matching the coins from combined ChangeHolders" in {
    Given("multiple holder with coins")
    When("combined")
    val combinedChangeHolder =
      ChangeHolder(Penny)  +
        ChangeHolder(Penny)  +
        ChangeHolder(SilverDollar)

    Then("the produced holder should hold all coins from the combined holders")
    combinedChangeHolder.coinCount(Penny) should be (2)
    combinedChangeHolder.coinCount(SilverDollar) should be (1)
  }

  it should "give the combined value of all coins held" in {
    Given("a holder with multiple coins ")
    val combinedChangeHolder =
      ChangeHolder(Penny)  +
        ChangeHolder(Penny)  +
        ChangeHolder(SilverDollar)

    Then("the holder's coin value should be the sum of the coins held")
    combinedChangeHolder.coinValue() should be (
      ChangeHolder(Penny).coinValue()  +
        ChangeHolder(Penny).coinValue()  +
        ChangeHolder(SilverDollar).coinValue()
    )
  }

  it must "have a coinValue with decimal precision of 2" in {
    Given("any ChangeHolder")
    val changeHolder = ChangeHolder(SilverDollar) +
    ChangeHolder(HalfDollar) +
    ChangeHolder(Quarter) +
    ChangeHolder(Dime) +
    ChangeHolder(Nickle) +
    ChangeHolder(Penny)

    Then("coinValue should have a precision of 2")
    changeHolder.coinValue() should be(1.91)

  }

  it must "hold a coin"  in {
    Given("a holder with a coin")
    val holderWithCoin: ChangeHolder = ChangeHolder(Quarter)

    And("an empty holder")
    val emptyHolder: ChangeHolder = ChangeHolder(None)

    When("the empty holder and the holder with a coin are summed")
    val holderWitCoinPlusEmptyHolder = holderWithCoin + emptyHolder
    val emptyHolderPlusHolderWitCoin = emptyHolder + holderWithCoin

    Then("the result should be equal to the holder with a coin")
    holderWitCoinPlusEmptyHolder should be(holderWithCoin)
    emptyHolderPlusHolderWitCoin should be(holderWithCoin)
  }
}
