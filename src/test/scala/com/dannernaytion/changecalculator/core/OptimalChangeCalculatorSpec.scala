package com.dannernaytion.changecalculator.core

import com.dannernaytion.changecalculator.core.Coins._
import org.scalatest.{FlatSpec, GivenWhenThen, ShouldMatchers}

trait OptimalChangeCalculatorSpec
  extends FlatSpec with GivenWhenThen with ShouldMatchers
    with OptimalChangeCalculatorComponent
{
  private val calculate: (Double, Set[Coin]) =>
    ChangeHolder = calculator.calculate _

  "An OptimalChangeCalculator" must
    "return change value equal to initial amount" in {

    Given(s"capable coin is available for selection")
    val capableCoins: Set[Coin] = Set(HalfDollar)

    val coinValue: Double = HalfDollar.value
    When(s"calculating for $coinValue")

    Then(s"optimal change value should be $coinValue")
    val optimalChange: ChangeHolder =
      calculate(coinValue, capableCoins)

    optimalChange.coinValue() should be(coinValue)
  }



  it should "not return change when calculating for less than 0.01" in {

    Given(s"all coins are available for selection")
    val coinsAvailable: Set[Coin] = Coins.allCoins

    val coinValue0: Double = 0.00
    val coinValuelt0: Double = -0.01
    When(s"calculating for $coinValue0 or $coinValuelt0")
    val optimalChange0, optimalChangelt0  =
      calculate(coinValue0, coinsAvailable)
      calculate(coinValuelt0, coinsAvailable)

    Then(s"change should be $None")
    optimalChange0 should be(ChangeHolder(None))
    optimalChangelt0 should be(ChangeHolder(None))

    val optimalChange: ChangeHolder = calculate(
      0.00,
      Coins.allCoins
    )
  }

  it should "return single coin multiple times" in {
    Given("multiple coins are available for selection")
    val coinsAvailable: Set[Coin] = Set(Nickle, Penny)

    And("calculating for a value divisible by only one of the coins available")
    val value: Double = Penny.value * 2

    Then("return should consist of the coin divisible")
    val expectedChange: ChangeHolder = ChangeHolder(Penny) + ChangeHolder(Penny)
    calculate(
      value,
      coinsAvailable
    ) should be(expectedChange)
  }

  it should "return optimal change" in {

    Given( "all coins are available for selections")
    val coinsAvailable = Coins.allCoins

    When("calculating for a value producible by many coin combinations")
    val value = 1.91

    Then("optimal change should be returned")
    val optimalChange = ChangeHolder(SilverDollar) +
      ChangeHolder(HalfDollar) +
      ChangeHolder(Quarter) +
      ChangeHolder(Dime) +
      ChangeHolder(Nickle) +
      ChangeHolder(Penny)

    calculate(
      value,
      coinsAvailable
    ) should be(optimalChange)
  }
}
