package com.dannernaytion.changecalculator.core

class RecursiveOptimalChangeCalculatorSpec extends
  OptimalChangeCalculatorSpec
{
  override val calculator: ChangeCalculator =
    new RecursiveOptimalChangeCalculator
}
