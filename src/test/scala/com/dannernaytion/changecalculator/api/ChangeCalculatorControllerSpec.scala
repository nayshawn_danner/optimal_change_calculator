package com.dannernaytion.changecalculator.api

import com.dannernaytion.changecalculator.application.{Change, ChangeCalculatorApp, ChangeDetail}
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpecLike, GivenWhenThen, ShouldMatchers}
import org.scalatra.test.scalatest.ScalatraFlatSpec

class ChangeCalculatorControllerSpec extends ScalatraFlatSpec  with ShouldMatchers
  with FlatSpecLike with MockFactory with GivenWhenThen {
  implicit val formats = DefaultFormats
  implicit val applicationMock = mock[ChangeCalculatorApp]
  def startApi: Unit = addServlet(new ChangeCalculatorController, "/*")

  "ChangeCalculator Api" should "return optimal change" in {

    Given("ChangeCalculator Api is started")
    startApi

    And("valid request is sent to the optimal change endpoint")
    val amount: Double = 1.78
    val validRequest: String = s"/optimal?amount=$amount"

    When("application is called with amount from request")
    val calculateOptimalCall = (applicationMock.calculateOptimal _).expects(*)

    And("application returns optimal change detail")
    calculateOptimalCall returning ChangeDetail(
      new Change(
        silver_dollar = 1,
        half_dollar = 1,
        quarter = 1,
        penny = 3
      ), amount) once()


    get(validRequest){
      Then("response status should be 200")
      status should equal(200)
      And("response body should have optimal change JSON")
      parse(body) should be(
        ("optimal_change" ->
          ("silver_dollar" -> 1) ~
            ("half_dollar" -> 1) ~
            ("quarter" -> 1) ~
            ("penny" -> 3)
          ) ~
          ("total" -> "$1.78")
      )
      And("application is called once with request amount")
    }
  }
  it should "return an amount required error" in {
    When("request does not include an amount")
    val requestWithoutAmount = "/optimal"
    get(requestWithoutAmount){
      Then("response status should be BAD_REQUEST")
      status should be (400)
      And("response body should contain an amount required error")
      val expectedBody=
        compact(
          render(
            "error" -> ("type" -> "AMOUNT_REQUIRED") ~
                       ("message" -> "amount required to calculate optimal change")))
      body should be (expectedBody)
    }
  }

  it should "return an invalid amount error" in {
    Given("request amount is not a valid number")
    val requestWithInvalidAmount = "/optimal?amount=invalid"
    get(requestWithInvalidAmount){
      Then("response status should be BAD_REQUEST")
      status should be (400)
      And("response body should contain an invalid amount error")
      val expectedBody=
        compact(
          render(
            "error" -> ("type" -> "INVALID_AMOUNT") ~
              ("message" -> "valid amount required to calculate optimal change")))
      body should be (expectedBody)
    }
  }
}
