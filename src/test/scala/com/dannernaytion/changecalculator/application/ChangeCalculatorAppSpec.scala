package com.dannernaytion.changecalculator.application

import com.dannernaytion.changecalculator.core.Coins._
import com.dannernaytion.changecalculator.core.{ChangeCalculator, ChangeHolder}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpecLike, GivenWhenThen}
import org.scalatra.test.scalatest.ScalatraFlatSpec

/**
  * Created by nayshawn on 6/12/16.
  */
class ChangeCalculatorAppSpec extends ScalatraFlatSpec with FlatSpecLike with MockFactory with GivenWhenThen{

  val optimalChangeCalculatorMock = mock[ChangeCalculator]
  val changeCalculatorApp = ChangeCalculatorApp(optimalChangeCalculatorMock)


  "A ChangeCalculatorApp" should "return optimal change detail" in {
    Given("calculateOptimal is called with valid amount")
    val validAmount = 1.91

    When("core is called with the valid amount")
    val calculateCall = (optimalChangeCalculatorMock.calculate _) expects(validAmount, *)

    And("core returns optimal change")
    calculateCall returning ChangeHolder(Penny)+
      ChangeHolder(Nickle)+
      ChangeHolder(Dime)+
      ChangeHolder(Quarter)+
      ChangeHolder(HalfDollar)+
      ChangeHolder(SilverDollar)

    Then("calculateOptimal should return optimal change detail")
    val changeDetail = new ChangeDetail(
      new Change(
        penny = 1,
        nickle = 1,
        dime = 1,
        quarter = 1,
        half_dollar = 1,
        silver_dollar = 1),
      validAmount)

    changeCalculatorApp.calculateOptimal(validAmount) should be (changeDetail)
  }
}

